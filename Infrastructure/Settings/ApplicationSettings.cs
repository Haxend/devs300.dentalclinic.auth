﻿namespace DentalClinic.Auth.Infrastructure.Settings
{
    public class ApplicationSettings
    {
        public string ConnectionString { get; set; }

        public string RmqPassword { get; set; }

        public string PostgresPassword { get; set; }

    }
}
