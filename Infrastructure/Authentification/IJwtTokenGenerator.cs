﻿using System.Globalization;

namespace DentalClinic.Auth.Application.Services;

public interface IJwtTokenGenerator
{
    string GenerateToken(Guid userId, string email, string? roleName, string? fio);
}