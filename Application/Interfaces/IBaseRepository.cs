﻿using DentalClinic.Auth.Domain;
using System.Linq.Expressions;

namespace DentalClinic.Auth.Application.Interfaces;

public interface IBaseRepository<T> where T : BaseEntity
{
    Task<Guid> CreateAsync(T entity);
    Task UpdateAsync(T entity);
    Task DeleteAsync(T entity);
    Task<T?> GetByIdAsync(Guid id);
    Task<bool> HasAnyByIdAsync(Guid id);
    Task<List<T>> GetAllAsync(CancellationToken cancellationToken = default);
    Task<List<T>> GetPaginatedListAsync(int offset, int count, Expression<Func<T, bool>>? expression = null, CancellationToken token = default);
    Task<int> GetTotalCount(CancellationToken token = default);
}