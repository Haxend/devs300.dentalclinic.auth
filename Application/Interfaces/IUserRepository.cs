﻿using DentalClinic.Auth.Domain;

namespace DentalClinic.Auth.Application.Interfaces;

public interface IUserRepository<T> : IBaseRepository<T> where T : User
{
    Task<User?> FindByEmailAsync(bool useAsNoTracking, string email);
    Task<bool> HasAnyByEmailAsync(string email);
    Task<bool> HasAnyByEmailExceptIdAsync(Guid id, string email);
    Task<bool> HasAnyByPhoneAsync(string phone);
}