﻿using DentalClinic.Auth.Domain;

namespace DentalClinic.Auth.Application.Interfaces
{
    public interface IUnitOfWork
    {
        IBaseRepository<T> GetRepository<T>() where T : BaseEntity;
        IUserRepository<T> GetUserRepository<T>() where T : User;
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
