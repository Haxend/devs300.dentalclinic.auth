﻿using AutoMapper;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Patient;

namespace DentalClinic.Auth.Application.Mappings
{
    public class PatientMapper : Profile
    {
        public PatientMapper()
        {
            // Create
            CreateMap<PatientCreateRequest, Patient>();
            CreateMap<Patient, PatientCreateResponse>();

            // Delete
            CreateMap<PatientDeleteRequest, Patient>();
            CreateMap<Patient, PatientDeleteResponse>();

            // Update
            CreateMap<PatientUpdateRequest, Patient>();
            CreateMap<Patient, PatientUpdateResponse>();

            // GetAll
            CreateMap<Patient, GettingPatientDto>();

            // Get
            CreateMap<Patient, PatientGetRequest>();
            CreateMap<Patient, PatientGetResponse>();
        }
    }
}
