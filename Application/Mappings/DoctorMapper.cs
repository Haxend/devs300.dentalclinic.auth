﻿using AutoMapper;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Doctor;

namespace DentalClinic.Auth.Application.Mappings
{
    public class DoctorMapper : Profile
    {
        public DoctorMapper() 
        {
            // Create
            CreateMap<DoctorCreateRequest, Doctor>();
            CreateMap<Doctor, DoctorCreateResponse>();

            // Delete
            CreateMap<DoctorDeleteRequest, Doctor>();
            CreateMap<Doctor, DoctorDeleteResponse>();

            // Update
            CreateMap<DoctorUpdateRequest, Doctor>();
            CreateMap<Doctor, DoctorUpdateResponse>();

            // GetAll
            CreateMap<Doctor, GettingDoctorDto>();

            // Get
            CreateMap<Doctor, DoctorGetRequest>();
            CreateMap<Doctor, DoctorGetResponse>();
        }
    }
}
