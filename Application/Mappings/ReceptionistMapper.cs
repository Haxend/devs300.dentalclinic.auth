﻿using AutoMapper;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Receptionist;

namespace DentalClinic.Auth.Application.Mappings
{
    public class ReceptionistMapper : Profile
    {
        public ReceptionistMapper()
        {
            // Create
            CreateMap<ReceptionistCreateRequest, Receptionist>();
            CreateMap<Receptionist, ReceptionistCreateResponse>();

            // Delete
            CreateMap<ReceptionistDeleteRequest, Receptionist>();
            CreateMap<Receptionist, ReceptionistDeleteResponse>();

            // Update
            CreateMap<ReceptionistUpdateRequest, Receptionist>();
            CreateMap<Receptionist, ReceptionistUpdateResponse>();

            // GetAll
            CreateMap<Receptionist, GettingReceptionistDto>();

            // Get
            CreateMap<Receptionist, ReceptionistGetRequest>();
            CreateMap<Receptionist, ReceptionistGetResponse>();
        }
    }
}
