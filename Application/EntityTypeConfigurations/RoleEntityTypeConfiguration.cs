﻿using DentalClinic.Auth.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DentalClinic.Auth.Application.EntityTypeConfigurations;

public class RoleTypeConfiguration : IEntityTypeConfiguration<Role>
{
    public void Configure(EntityTypeBuilder<Role> builder)
    {
        builder.HasIndex(i => i.Name)
            .IsUnique();

        builder.HasMany<User>()
            .WithOne(user => user.Role)
            .HasForeignKey(e => e.RoleId);
    }
}