﻿using System.Security.Cryptography;
using System.Text;

namespace DentalClinic.Auth.Application.Services
{
    public class PasswordGeneration
    {
        public Tuple<string, string> GeneratePasswordAndHash(int length = 10)
        {
            const string validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+";
            StringBuilder sb = new StringBuilder();
            Random rnd = new Random();

            for (int i = 0; i < length; i++)
            {
                int index = rnd.Next(0, validChars.Length);
                sb.Append(validChars[index]);
            }

            string password = sb.ToString();

            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(password));

                StringBuilder hashBuilder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    hashBuilder.Append(bytes[i].ToString("x2"));
                }
                string hashedPassword = hashBuilder.ToString();
                return new Tuple<string, string>(password, hashedPassword);
            }
        }
    }
}
