﻿using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Application.Services;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Auth.Requests;
using DentalClinic.Common.DentalClinic.Contracts.Auth.Responses;
using DentalClinic.Common.Exceptions;
using MassTransit;
using Microsoft.AspNetCore.Identity;

namespace DentalClinic.Auth.Application.Consumers.Auth;

public class AuthenticateConsumer : IConsumer<Authenticate>
{
    private readonly IJwtTokenGenerator _jwtTokenGenerator;
    private readonly IUserRepository<User> _userRepository;

    public AuthenticateConsumer(IJwtTokenGenerator jwtTokenGenerator, IUserRepository<User> userRepository)
    {
        _jwtTokenGenerator = jwtTokenGenerator;
        _userRepository = userRepository;
    }
    
    public async Task Consume(ConsumeContext<Authenticate> context)
    {
        var request = context.Message;
        var user = await _userRepository.FindByEmailAsync(true, request.Email);

        //var users = await _userRepository.GetAllAsync();

        if (user == null)
            throw new NotFoundException($"User with email {request.Email} not found");
        
        var result = new PasswordHasher<User>().VerifyHashedPassword
            (user, user.PasswordHash, request.Password);

        if (result == PasswordVerificationResult.Failed)
            throw new WrongPasswordException($"User with {request.Email} sent wrong password");
        
        await context.RespondAsync(new AuthenticateResult
        {
            AccessToken = _jwtTokenGenerator.GenerateToken(user.Id, user.Email, user.Role.Name, user.LastName+" "+user.FirstName+" "+user.FatherName)
        });
    }
}