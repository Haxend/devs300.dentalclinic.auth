﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Common.Exceptions;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers
{
    public class UpdatePatientConsumer : IConsumer<PatientUpdateRequest>
    {
        private readonly IUserRepository<Patient> _patientRepository;
        private readonly IMapper _mapper;

        public UpdatePatientConsumer(IUserRepository<Patient> patientRepository, IMapper mapper)
        {
            _patientRepository = patientRepository;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<PatientUpdateRequest> context)
        {
            var request = context.Message;
            var patient = await _patientRepository.GetByIdAsync(request.Id);

            if (patient == null)
                throw new NotFoundException($"Пользователь с ID {request.Id} не найден");

            if (await _patientRepository.HasAnyByEmailExceptIdAsync(request.Id, request.Email))
                throw new ConflictException($"Пользователь с {request.Email} уже существует");

            _mapper.Map(request, patient);
            patient.RoleId = Guid.Parse("29c3471d-5cb2-4cab-b815-3ed6718268a0");
            await _patientRepository.UpdateAsync(patient);

            //Синхронизируем c БД DentalClinic.Business
            await context.Publish(new SyncPatientUpdate
            {
                OuterId = patient.Id,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                PhoneNumber = request.PhoneNumber,
                PasswordHash = patient.PasswordHash,
                Allergy = request.Allergy,
                RoleId = patient.RoleId
            });

            await context.RespondAsync(_mapper.Map<PatientUpdateResponse>(patient));
        }
    }
}
