﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Common.Exceptions;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers
{
    public class GetPatientConsumer : IConsumer<PatientGetRequest>
    {
        private readonly IUserRepository<Patient> _patientRepository;
        private readonly IMapper _mapper;

        public GetPatientConsumer(IUserRepository<Patient> patientRepository,
                                 IMapper mapper)
        {
            _patientRepository = patientRepository;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<PatientGetRequest> context)
        {
            var request = context.Message;

            var patient = await _patientRepository.GetByIdAsync(request.Id);

            if (patient == null)
                throw new NotFoundException($"Пользователь не найден");

            await context.RespondAsync(_mapper.Map<PatientGetResponse>(patient));
        }
    }
}
