﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Patient;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers
{
    public class GetAllPatientConsumer : IConsumer<PatientGetListRequest>
    {
        private readonly IUserRepository<Patient> _patientRepository;
        private readonly IMapper _mapper;

        public GetAllPatientConsumer(IUserRepository<Patient> patientRepository,
                                 IMapper mapper)
        {
            _patientRepository = patientRepository;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<PatientGetListRequest> context)
        {
            var request = context.Message;

            var users = await _patientRepository.GetPaginatedListAsync
                (request.Offset, request.Count, e => e.RoleId == Guid.Parse("29c3471d-5cb2-4cab-b815-3ed6718268a0"));

            await context.RespondAsync(new PatientGetListResponse
            {
                Elements = _mapper.Map<List<GettingPatientDto>>(users),
                TotalCount = await _patientRepository.GetTotalCount()
            });
        }
    }
}
