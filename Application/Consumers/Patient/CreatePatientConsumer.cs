﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Application.Services;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Common.Exceptions;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers;

public class CreatePatientConsumer : IConsumer<PatientCreateRequest>
{
    private readonly PasswordGeneration _passwordGeneration;
    private readonly IUserRepository<Patient> _patientRepository;
    private readonly IMapper _mapper;

    public CreatePatientConsumer(IUserRepository<Patient> patientRepository, IMapper mapper, PasswordGeneration passwordGeneration)
    {
        _patientRepository = patientRepository;
        _mapper = mapper;
        _passwordGeneration = passwordGeneration;
    }
    
    public async Task Consume(ConsumeContext<PatientCreateRequest> context)
    {
        var request = context.Message;

        if (await _patientRepository.HasAnyByPhoneAsync(request.PhoneNumber))
            throw new ConflictException("Пользователь с таким номером телефона уже существует!");
        var patient = _mapper.Map<Patient>(request);

        patient.Id = Guid.NewGuid();
        var pass = _passwordGeneration.GeneratePasswordAndHash();
        patient.PasswordHash = pass.Item2;
        patient.RoleId = Guid.Parse("29c3471d-5cb2-4cab-b815-3ed6718268a0");

        var id = await _patientRepository.CreateAsync(patient);

        //Синхронизируем c БД DentalClinic.Business
        await context.Publish(new SyncPatientCreate
        {
            OuterId = patient.Id,
            Email = request.Email,
            FirstName = request.FirstName,
            LastName = request.LastName,
            PhoneNumber = request.PhoneNumber,
            PasswordHash = patient.PasswordHash,
            Allergy = request.Allergy,
            RoleId = patient.RoleId
        });

        // todo: Отправляем уведомление через DentalClinic.Notification
        //await context.Publish(new UserCreatedNotification
        //{
        //    Email = request.Email,
        //    FirstName = request.FirstName,
        //    LastName = request.LastName,
        //});

        await context.RespondAsync(_mapper.Map<DoctorCreateResponse>(patient));
    }
}