﻿using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Common.Exceptions;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers;

public class DeletePatientConsumer : IConsumer<PatientDeleteRequest>
{
    private readonly IUserRepository<Patient> _patientRepository;

    public DeletePatientConsumer(IUserRepository<Patient> patientRepository)
    {
        _patientRepository = patientRepository;
    }

    public async Task Consume(ConsumeContext<PatientDeleteRequest> context)
    {
        var request = context.Message;

        var patient = await _patientRepository.GetByIdAsync(request.Id);

        if (patient == null)
            throw new NotFoundException($"Пользователь не найден");

        await _patientRepository.DeleteAsync(patient);

        //Синхронизируем c БД DentalClinic.Business
        await context.Publish(new SyncPatientDelete
        {
            Id = patient.Id,
        });

        await context.RespondAsync(new PatientDeleteResponse());
    }
}