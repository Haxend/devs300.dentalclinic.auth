﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Receptionist;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers
{
    public class GetAllReceptionistConsumer : IConsumer<ReceptionistGetListRequest>
    {
        private readonly IUserRepository<Receptionist> _receptionistRepository;
        private readonly IMapper _mapper;

        public GetAllReceptionistConsumer(IUserRepository<Receptionist> receptionistRepository, IMapper mapper)
        {
            _receptionistRepository = receptionistRepository;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<ReceptionistGetListRequest> context)
        {
            var request = context.Message;

            var users = await _receptionistRepository.GetPaginatedListAsync
                (request.Offset, request.Count, e => e.RoleId == Guid.Parse("6cefb619-7056-4004-9ef8-c96a988774da"));

            await context.RespondAsync(new ReceptionistGetListResponse
            {
                Elements = _mapper.Map<List<GettingReceptionistDto>>(users),
                TotalCount = await _receptionistRepository.GetTotalCount()
            });
        }
    }
}
