﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Common.Contracts.Receptionist;
using DentalClinic.Common.Exceptions;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers;

public class GetReceptionistConsumer : IConsumer<ReceptionistGetRequest>
{
    private readonly IUserRepository<Receptionist> _receptionistRepository;
    private readonly IMapper _mapper;

    public GetReceptionistConsumer(IUserRepository<Receptionist> receptionistRepository, IMapper mapper)
    {
        _receptionistRepository = receptionistRepository;
        _mapper = mapper;
    }

    public async Task Consume(ConsumeContext<ReceptionistGetRequest> context)
    {
        var request = context.Message;

        var receptionist = await _receptionistRepository.GetByIdAsync(request.Id);

        if (receptionist == null)
            throw new NotFoundException($"Пользователь не найден");

        await context.RespondAsync(_mapper.Map<PatientGetResponse>(receptionist));
    }
}