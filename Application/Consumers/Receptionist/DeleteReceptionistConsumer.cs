﻿using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Common.Contracts.Receptionist;
using DentalClinic.Common.Exceptions;
using MassTransit;
using System.Numerics;

namespace DentalClinic.Auth.Application.Consumers;

public class DeleteReceptionistConsumer : IConsumer<ReceptionistDeleteRequest>
{
    private readonly IUserRepository<Receptionist> _receptionistRepository;

    public DeleteReceptionistConsumer(IUserRepository<Receptionist> receptionistRepository)
    {
        _receptionistRepository = receptionistRepository;
    }

    public async Task Consume(ConsumeContext<ReceptionistDeleteRequest> context)
    {
        var request = context.Message;

        var receptionist = await _receptionistRepository.GetByIdAsync(request.Id);

        if (receptionist == null)
            throw new NotFoundException($"Пользователь не найден");

        await _receptionistRepository.DeleteAsync(receptionist);

        //Синхронизируем c БД DentalClinic.Business
        await context.Publish(new SyncReceptionistDelete
        {
            Id = request.Id,
        });

        await context.RespondAsync(new PatientDeleteResponse());
    }
}