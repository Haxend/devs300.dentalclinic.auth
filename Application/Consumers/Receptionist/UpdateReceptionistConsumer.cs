﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Common.Contracts.Receptionist;
using DentalClinic.Common.Exceptions;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers
{
    public class UpdateReceptionistConsumer : IConsumer<ReceptionistUpdateRequest>
    {
        private readonly IUserRepository<Receptionist> _receptionistRepository;
        private readonly IMapper _mapper;

        public UpdateReceptionistConsumer(IUserRepository<Receptionist> receptionistRepository, IMapper mapper)
        {
            _receptionistRepository = receptionistRepository;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<ReceptionistUpdateRequest> context)
        {
            var request = context.Message;
            var receptionist = await _receptionistRepository.GetByIdAsync(request.Id);

            if (receptionist == null)
                throw new NotFoundException($"Пользователь с ID {request.Id} не найден");

            if (await _receptionistRepository.HasAnyByEmailExceptIdAsync(request.Id, request.Email))
                throw new ConflictException($"Пользователь с {request.Email} уже существует");

            if (await _receptionistRepository.HasAnyByPhoneAsync(request.PhoneNumber))
                throw new ConflictException($"Пользователь с {request.Email} уже существует");

            _mapper.Map(request, receptionist);
            receptionist.RoleId = Guid.Parse("6cefb619-7056-4004-9ef8-c96a988774da");
            await _receptionistRepository.UpdateAsync(receptionist);

            //Синхронизируем c БД DentalClinic.Business
            await context.Publish(new SyncReceptionistUpdate
            {
                OuterId = receptionist.Id,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                PhoneNumber = request.PhoneNumber,
                PasswordHash = receptionist.PasswordHash,
                RoleId = receptionist.RoleId
            });

            await context.RespondAsync(_mapper.Map<PatientUpdateResponse>(receptionist));
        }
    }
}
