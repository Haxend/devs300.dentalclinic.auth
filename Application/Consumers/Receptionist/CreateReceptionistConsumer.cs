﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Application.Services;
using DentalClinic.Auth.Domain;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Common.Contracts.Receptionist;
using DentalClinic.Common.Exceptions;
using MassTransit;
using System.Numerics;

namespace DentalClinic.Auth.Application.Consumers;

public class CreateReceptionistConsumer : IConsumer<ReceptionistCreateRequest>
{
    private readonly PasswordGeneration _passwordGeneration;
    private readonly IUserRepository<Receptionist> _receptionistRepository;
    private readonly IMapper _mapper;

    public CreateReceptionistConsumer(IUserRepository<Receptionist> receptionistRepository, IMapper mapper, PasswordGeneration passwordGeneration)
    {
        _receptionistRepository = receptionistRepository;
        _mapper = mapper;
        _passwordGeneration = passwordGeneration;
    }

    public async Task Consume(ConsumeContext<ReceptionistCreateRequest> context)
    {
        var request = context.Message;

        if (await _receptionistRepository.HasAnyByPhoneAsync(request.PhoneNumber))
            throw new ConflictException("Пользователь с таким номером телефона уже существует!");
        var receptionist = _mapper.Map<Receptionist>(request);

        receptionist.Id = Guid.NewGuid();
        var pass = _passwordGeneration.GeneratePasswordAndHash();
        receptionist.PasswordHash = pass.Item2;
        receptionist.RoleId = Guid.Parse("6cefb619-7056-4004-9ef8-c96a988774da");

        var id = await _receptionistRepository.CreateAsync(receptionist);

        //Синхронизируем c БД DentalClinic.Business
        await context.Publish(new SyncReceptionistCreate
        {
            OuterId = receptionist.Id,
            Email = request.Email,
            FirstName = request.FirstName,
            LastName = request.LastName,
            PhoneNumber = request.PhoneNumber,
            PasswordHash = receptionist.PasswordHash,
            RoleId = receptionist.RoleId
        });

        // todo: Отправляем уведомление через DentalClinic.Notification
        //await context.Publish(new UserCreatedNotification
        //{
        //    Email = request.Email,
        //    FirstName = request.FirstName,
        //    LastName = request.LastName,
        //});

        await context.RespondAsync(_mapper.Map<ReceptionistCreateResponse>(receptionist));
    }
}