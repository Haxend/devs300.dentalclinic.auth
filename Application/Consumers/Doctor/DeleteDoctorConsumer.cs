﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Common.Exceptions;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers
{
    public class DeleteDoctorConsumer : IConsumer<DoctorDeleteRequest>
    {
        private readonly IUserRepository<Domain.Doctor> _doctorRepository;
        private readonly IMapper _mapper;

        public DeleteDoctorConsumer(IUserRepository<Domain.Doctor> doctorRepository,
                                    IMapper mapper)
        {
            _doctorRepository = doctorRepository;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<DoctorDeleteRequest> context)
        {
            var request = context.Message;

            var doctor = await _doctorRepository.GetByIdAsync(request.Id);

            if (doctor == null)
                throw new NotFoundException($"Пользователь не найден");

            await _doctorRepository.DeleteAsync(doctor);

            //Синхронизируем c БД DentalClinic.Business
            await context.Publish(new SyncDoctorDelete
            {
                Id = doctor.Id,
            });

            await context.RespondAsync(new DoctorDeleteResponse());
        }
    }
}
