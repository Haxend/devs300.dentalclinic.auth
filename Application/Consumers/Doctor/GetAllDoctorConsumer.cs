﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Common.Contracts.Doctor;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers
{
    public class GetAllDoctorConsumer : IConsumer<DoctorGetListRequest>
    {
        private readonly IUserRepository<Domain.Doctor> _doctorRepository;
        private readonly IMapper _mapper;

        public GetAllDoctorConsumer(IUserRepository<Domain.Doctor> doctorRepository,
                                 IMapper mapper)
        {
            _doctorRepository = doctorRepository;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<DoctorGetListRequest> context)
        {
            var request = context.Message;

            var users = await _doctorRepository.GetPaginatedListAsync
                (request.Offset, request.Count, e => e.RoleId == Guid.Parse("60e26b53-f985-4cd8-ad5f-3f75a56368ce"));

            await context.RespondAsync(new DoctorGetListResponse
            {
                Elements = _mapper.Map<List<GettingDoctorDto>>(users),
                TotalCount = await _doctorRepository.GetTotalCount()
            });
        }
    }
}
