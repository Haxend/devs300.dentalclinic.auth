﻿using AutoMapper;
using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Common.Exceptions;
using MassTransit;

namespace DentalClinic.Auth.Application.Consumers
{
    public class UpdateDoctorConsumer : IConsumer<DoctorUpdateRequest>
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository<Domain.Doctor> _doctorRepository;

        public UpdateDoctorConsumer(IMapper mapper, IUserRepository<Domain.Doctor> doctorRepository)
        {
            _mapper = mapper;
            _doctorRepository = doctorRepository;
        }

        public async Task Consume(ConsumeContext<DoctorUpdateRequest> context)
        {
            var request = context.Message;
            var doctor = await _doctorRepository.GetByIdAsync(request.Id);

            if (doctor == null)
                throw new NotFoundException($"Пользователь с ID {request.Id} не найден");

            if (await _doctorRepository.HasAnyByEmailExceptIdAsync(request.Id, request.Email))
                throw new ConflictException($"Пользователь с {request.Email} уже существует");


            _mapper.Map(request, doctor);
            doctor.RoleId = Guid.Parse("29c3471d-5cb2-4cab-b815-3ed6718268a0");
            await _doctorRepository.UpdateAsync(doctor);

            //Синхронизируем c БД DentalClinic.Business
            await context.Publish(new SyncDoctorUpdate
            {
                OuterId = doctor.Id,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                PhoneNumber = request.PhoneNumber,
                PasswordHash = doctor.PasswordHash,
                Qualification = request.Qualification,
                RoleId = doctor.RoleId
            });

            await context.RespondAsync(_mapper.Map<DoctorUpdateResponse>(doctor));
        }
    }
}
