﻿using DentalClinic.Auth.Application;
using DentalClinic.Auth.Application.Consumers;
using DentalClinic.Auth.Application.Consumers.Auth;
using DentalClinic.Auth.Persistence;
using MassTransit;
using Microsoft.OpenApi.Models;

namespace DentalClinic.Auth.Presentation
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigurePersistence(Configuration);
            services.ConfigureApplication(Configuration);
            services.AddConfigurations(Configuration);

            services.ConfigureApiBehavior();
            services.ConfigureCorsPolicy();

            services.AddMvc();

            services.AddControllers();
            services.AddEndpointsApiExplorer();

            services.AddMassTransit(x =>
            {
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(Configuration["RabbitMQ:Host"], h =>
                    {
                        h.Username(Configuration["RabbitMQ:Username"]);
                        h.Password(Configuration["RabbitMQ:Password"]);
                    });
                    cfg.ConfigureEndpoints(context);
                });

                //Auth
                x.AddConsumer<AuthenticateConsumer>();

                // Patient
                x.AddConsumer<CreatePatientConsumer>();
                x.AddConsumer<GetPatientConsumer>();
                x.AddConsumer<GetAllPatientConsumer>();
                x.AddConsumer<UpdatePatientConsumer>();
                x.AddConsumer<DeletePatientConsumer>();

                //Doctor
                x.AddConsumer<CreateDoctorConsumer>();
                x.AddConsumer<GetDoctorConsumer>();
                x.AddConsumer<GetAllDoctorConsumer>();
                x.AddConsumer<UpdateDoctorConsumer>();
                x.AddConsumer<DeleteDoctorConsumer>();

                //Receptionist
                x.AddConsumer<CreateReceptionistConsumer>();
                x.AddConsumer<GetReceptionistConsumer>();
                x.AddConsumer<GetAllReceptionistConsumer>();
                x.AddConsumer<UpdateReceptionistConsumer>();
                x.AddConsumer<DeleteReceptionistConsumer>();
            });

            services.AddSwaggerGen(option =>
            {
                option.SwaggerDoc("v1", new OpenApiInfo { Title = "DenatalClinic.Auth API", Version = "v1" });
                option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please enter a valid token",
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });
                option.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type=ReferenceType.SecurityScheme,
                            Id="Bearer"
                        }
                    },
                    new string[]{}
                }
            });
            });


        }

        public void Configure(WebApplication app, IWebHostEnvironment env)
        {
            app.UseCors();
            app.MapControllers();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseRouting();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "DenatalClinic.Auth API V1");
            });
        }
    }
}
