﻿namespace DentalClinic.Auth.Domain.Enum;

public static class Roles
{
    public const string Client = "Client";
    public const string Admin = "Admin";
    public const string SuperAdmin = "SuperAdmin";
    public const string Doctor = "doctor";
    public const string Receptionist = "receptionist";

    public static Dictionary<string, Guid> GetAllRolesWithIds()
    {
        return new Dictionary<string, Guid>
    {
        {Client, Guid.Parse("29c3471d-5cb2-4cab-b815-3ed6718268a0")},
        {Admin, Guid.Parse("60e26b53-f985-4cd8-ad5f-3f75a56368ce")},
        {SuperAdmin, Guid.Parse("6cefb619-7056-4004-9ef8-c96a988774da")},
        {Doctor, Guid.Parse("a41e367b-65c2-4505-b14d-6c14858d802c")},
        {Receptionist, Guid.Parse("4e9fa974-7e8d-404e-b277-a234dfb0ea1f")}
    };
    }
}

