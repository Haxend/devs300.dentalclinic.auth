﻿namespace DentalClinic.Auth.Domain;

public abstract class User : BaseEntity
{
    public string? FirstName { get; set; }
    public string? FatherName { get; set; }
    public string LastName { get; set; } = null!;
    public string? Email { get; set; }
    public string? PasswordHash { get; set; }
    public string? PhoneNumber { get; set; }
    public virtual Role Role { get; set; }
    public Guid RoleId { get; set; }
}
