﻿using Microsoft.AspNetCore.Identity;

namespace DentalClinic.Auth.Domain;

public class Role : IdentityRole<Guid>
{
    public string? Description { get; set; }
}
