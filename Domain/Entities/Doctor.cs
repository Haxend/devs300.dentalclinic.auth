﻿namespace DentalClinic.Auth.Domain;
public class Doctor : User
{
    public string Qualification { get; set; }
}
