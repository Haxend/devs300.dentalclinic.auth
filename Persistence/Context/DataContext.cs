﻿using DentalClinic.Auth.Domain;
using Microsoft.EntityFrameworkCore;

namespace DentalClinic.Auth.Persistence.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            
        }

        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Role> Roles { get; set; } = null!;
        public DbSet<Doctor> Doctors { get; set; } = null!;
        public DbSet<Receptionist> Receptionists { get; set; } = null!;
        public DbSet<Patient> Patients { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            FillDefaultData(modelBuilder);
        }

        private static void FillDefaultData(ModelBuilder builder)
        {
            var rolesWithIds = Domain.Enum.Roles.GetAllRolesWithIds();

            var superAdminRole = new Role
            {
                Id = rolesWithIds[Domain.Enum.Roles.SuperAdmin],
                Name = Domain.Enum.Roles.SuperAdmin,
            };

            var adminRole = new Role
            {
                Id = rolesWithIds[Domain.Enum.Roles.Admin],
                Name = Domain.Enum.Roles.Admin,
            };

            var clientRole = new Role
            {
                Id = rolesWithIds[Domain.Enum.Roles.Client],
                Name = Domain.Enum.Roles.Client,
            };

            var doctorRole = new Role
            {
                Id = rolesWithIds[Domain.Enum.Roles.Doctor],
                Name = Domain.Enum.Roles.Doctor,
            };

            var receptionistRole = new Role
            {
                Id = rolesWithIds[Domain.Enum.Roles.Receptionist],
                Name = Domain.Enum.Roles.Receptionist,
            };

            builder.Entity<Role>().HasData([
                clientRole,
                adminRole,
                superAdminRole,
                doctorRole,
                receptionistRole
            ]);

            builder.Entity<Doctor>().HasData([            
            new Doctor()
            {
                Id = Guid.Parse("18033de0-fa1a-470a-895f-e2ae6b51776f"),
                CreatedAt = DateTime.UtcNow,
                FatherName = "Иванович",
                FirstName = "Иван",
                LastName = "Иванов",
                Email = "doctor1@mail.ru",
                PasswordHash = "AQAAAAIAAYagAAAAEHpX9YRUx2LHWG4N5dxWszz3Cgn1mdFl6f5l3slTKrMmqFodCjz7abc564LoKqS98w==", //root
                PhoneNumber = "+792266585478",
                Qualification = "Стоматолог-хирург",
                RoleId = doctorRole.Id,
            },
            new Doctor()
            {
                Id = Guid.Parse("a095d80a-65e2-4022-ad1b-4b2f251506bb"),
                CreatedAt = DateTime.UtcNow,
                FatherName = "Петрович",
                FirstName = "Пётр",
                LastName = "Петров",
                Email = "doctor2@mail.ru",
                PasswordHash = "AQAAAAIAAYagAAAAEHpX9YRUx2LHWG4N5dxWszz3Cgn1mdFl6f5l3slTKrMmqFodCjz7abc564LoKqS98w==", //root
                PhoneNumber = "+79543335552",
                Qualification = "Стоматолог-терапевт",
                RoleId = doctorRole.Id,
            },
            new Doctor()
            {
                Id = Guid.Parse("dd15f0c3-9838-4530-b6b3-17e62a688d5a"),
                CreatedAt = DateTime.UtcNow,
                FatherName = "Валерьевич",
                FirstName = "Игорь",
                LastName = "Сидоров",
                Email = "user2@mail.ru",
                PasswordHash = "AQAAAAIAAYagAAAAEHpX9YRUx2LHWG4N5dxWszz3Cgn1mdFl6f5l3slTKrMmqFodCjz7abc564LoKqS98w==", //root
                PhoneNumber = "+79501234567",
                Qualification = "Стоматолог-ортопед",
                RoleId = doctorRole.Id
            }
            ]);

            builder.Entity<Patient>().HasData([
            new Patient()
            {
                Id = Guid.Parse("d9ffe96a-5161-4e83-9c2c-885b5b9ec575"),
                FatherName = "Константинович",
                FirstName = "Илья",
                LastName = "Дунаевский",
                Email = "cenetik@gmail.com",
                PasswordHash = "AQAAAAIAAYagAAAAEHpX9YRUx2LHWG4N5dxWszz3Cgn1mdFl6f5l3slTKrMmqFodCjz7abc564LoKqS98w==", //root
                PhoneNumber = "+79228888887",
                Allergy = "Новокаин",
                RoleId = clientRole.Id,
            },
            new Patient()
            {
                Id = Guid.Parse("a70e7958-bb2b-43bc-a2c8-67044c5bd500"),
                FatherName = "Кириллович",
                FirstName = "Борис",
                LastName = "Бубнов",
                Email = "none3@mail.ru",
                PasswordHash = "AQAAAAIAAYagAAAAEHpX9YRUx2LHWG4N5dxWszz3Cgn1mdFl6f5l3slTKrMmqFodCjz7abc564LoKqS98w==", //root
                PhoneNumber = "+79528889788",
                Allergy = null,
                RoleId = clientRole.Id,
            },
            new Patient()
            {
                Id = Guid.Parse("a6455f7c-c26b-4843-b900-bb4e295fffa5"),
                FatherName = "Семёнович",
                FirstName = "Семён",
                LastName = "Горбунков",
                Email = "zorro@mail.ru",
                PasswordHash = "AQAAAAIAAYagAAAAEHpX9YRUx2LHWG4N5dxWszz3Cgn1mdFl6f5l3slTKrMmqFodCjz7abc564LoKqS98w==", //root
                PhoneNumber = "+79527770102",
                Allergy = "Скандонест",
                RoleId = clientRole.Id,
            }]);

            builder.Entity<Receptionist>().HasData([
            new Receptionist()
            {
                Id = Guid.NewGuid(),
                FatherName = "Филлиповна",
                FirstName = "Екатерина",
                LastName = "Ерёменко",
                Email = "receptionist@mail.ru",
                PasswordHash = "AQAAAAIAAYagAAAAEHpX9YRUx2LHWG4N5dxWszz3Cgn1mdFl6f5l3slTKrMmqFodCjz7abc564LoKqS98w==", //root
                PhoneNumber = "+79089845335",
                RoleId = receptionistRole.Id,
            }]);
        }
    }
}
