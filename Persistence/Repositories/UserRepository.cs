﻿using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Auth.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace DentalClinic.Auth.Persistence.Repositories;

public class UserRepository<T>(DataContext context) : BaseRepository<T>(context), IUserRepository<T> where T : User
{
    public async Task<User?> FindByEmailAsync(bool useAsNoTracking, string email)
    {
        if (useAsNoTracking)
            return await context.Users
                .Include(e => e.Role)
                .SingleOrDefaultAsync(x => x.Email == email);

        return await context.Users
            .Include(e => e.Role)
            .AsNoTracking()
            .SingleOrDefaultAsync(x => x.Email == email);
    }

    public async Task<bool> HasAnyByEmailAsync(string email)
    {
        return await context.Users
            .AsNoTracking()
            .AnyAsync(x => x.Email == email);
    }

    public async Task<bool> HasAnyByEmailExceptIdAsync(Guid id, string email)
    {
        return await context.Users
            .AsNoTracking()
            .AnyAsync(x => x.Id != id && x.Email.ToLower() == email);
    }

    public async Task<bool> HasAnyByPhoneAsync(string phone)
    {
        return await context.Users
            .AsNoTracking()
            .AnyAsync(x => x.PhoneNumber == phone);
    }
}