﻿using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Auth.Persistence.Context;

namespace DentalClinic.Auth.Persistence.Repositories
{
    public class UnitOfWork<TContext> : IUnitOfWork where TContext : DataContext
    {
    
        private readonly TContext context;
        private readonly Dictionary<Type,object> repositories = new Dictionary<Type,object>();

        public UnitOfWork(TContext context)
        {
            this.context = context;
        }

        public IBaseRepository<T> GetRepository<T>() where T : BaseEntity
        {
            var type = typeof(T);
            if(!repositories.ContainsKey(type))
            {
                repositories[type] = new BaseRepository<T>(context);
            }
            return (IBaseRepository<T>)repositories[type];
        }

        public IUserRepository<T> GetUserRepository<T>() where T : User
        {
            var type = typeof(T);
            if (!repositories.ContainsKey(type))
            {
                repositories[type] = new UserRepository<T>(context);
            }
            return (IUserRepository<T>)repositories[type];
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await context.SaveChangesAsync();
        }
    }
}
