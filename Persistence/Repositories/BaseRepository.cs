﻿using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Domain;
using DentalClinic.Auth.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace DentalClinic.Auth.Persistence.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly DataContext Context;

        public BaseRepository(DataContext context)
        {
            Context = context;
        }

        public async Task<Guid> CreateAsync(T entity)
        {
            entity.CreatedAt = DateTimeOffset.UtcNow;
            await Context.AddAsync(entity);
            await Context.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(T entity)
        {
            entity.UpdatedAt = DateTimeOffset.UtcNow;
            Context.Update(entity);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            Context.Remove(entity);
            await Context.SaveChangesAsync();
        }

        public async Task<T?> GetByIdAsync(Guid id)
        {
            return await Context.Set<T>().SingleOrDefaultAsync(x => x.Id == id);
        } 

        public async Task<bool> HasAnyByIdAsync(Guid id)
        {
            return await Context.Set<T>().AnyAsync(x => x.Id == id);
        }

        public async Task<List<T>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await Context.Set<T>().ToListAsync(cancellationToken);
        }

        public async Task<List<T>> GetPaginatedListAsync(int offset, int count, Expression<Func<T, bool>>? expression, CancellationToken token)
        {
            var query = expression == null ? Context.Set<T>() : Context.Set<T>().Where(expression);
            return await query.Skip(offset).Take(count).ToListAsync(token);
        }

        public async Task<int> GetTotalCount(CancellationToken token)
        {
            return await Context.Set<T>().CountAsync(token);
        }
    }
}
