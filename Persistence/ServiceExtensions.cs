﻿using DentalClinic.Auth.Application.Interfaces;
using DentalClinic.Auth.Application.Services;
using DentalClinic.Auth.Domain;
using DentalClinic.Auth.Infrastructure;
using DentalClinic.Auth.Infrastructure.Settings;
using DentalClinic.Auth.Persistence.Context;
using DentalClinic.Auth.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DentalClinic.Auth.Persistence
{
    public static class ServiceExtensions
    {
        public static void ConfigurePersistence(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("PostgreSQL");
            services.AddDbContext<DataContext>(opt => { opt.UseNpgsql(connectionString); });

            //Заранее создал класс настроек проекта ApplicationSettings
            var applicationSettings = configuration.Get<ApplicationSettings>();

            services.AddSingleton(applicationSettings)
                    .AddSingleton((IConfigurationRoot)configuration)
                    .InstallServices();
            services.Configure<JwtOptions>(configuration.GetSection("JwtOptions"));
        }

        public static IServiceCollection InstallServices(this IServiceCollection services)
        {
            services.AddTransient<PasswordGeneration>();

            services.AddTransient<IUnitOfWork, UnitOfWork<DataContext>>();
            services.AddTransient<IUserRepository<User>, UserRepository<User>>();
            services.AddTransient<IUserRepository<Doctor>, UserRepository<Doctor>>();
            services.AddTransient<IUserRepository<Patient>, UserRepository<Patient>>();
            services.AddTransient<IUserRepository<Receptionist>, UserRepository<Receptionist>>();

            services.AddTransient<IBaseRepository<User>, BaseRepository<User>>();
            services.AddTransient<IBaseRepository<Doctor>, BaseRepository<Doctor>>();
            services.AddTransient<IBaseRepository<Patient>, BaseRepository<Patient>>();
            services.AddTransient<IBaseRepository<Receptionist>, BaseRepository<Receptionist>>();



            return services;
        }
    }
}
